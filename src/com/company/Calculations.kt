package com.company

object Calculations {

    fun positionGeometricCenter(point: Array<Point2D?>): Point2D {

        var x = 0.0
        var y = 0.0

        for (i in point) {
            x += i.pointX
            y += i.pointY
        }
        return Point2D(x / point.size, y / point.size)
    }

    fun positionCenterOfMass(materialPoints: Array<MaterialPoint2D?>): Point2D {

        var x = 0.0
        var y = 0.0
        var mass = 0.0

        for (i in materialPoints) {
            x += i.pointX * i.mass
            y += i.pointY * i.mass
            mass += i.mass
        }
        return MaterialPoint2D(x / mass, y / mass, mass)
    }
}
