package com.company

open class Point2D(pointX: Double, pointY: Double) {

    var pointX: Double = 0.toDouble()
        internal set
    var pointY: Double = 0.toDouble()
        internal set

    init {
        this.pointX = pointX
        this.pointY = pointY
    }

    override fun toString(): String {
        return "Point2D{" +
                "pointX=" + pointX +
                ", pointY=" + pointY +
                '}'.toString()
    }
}
