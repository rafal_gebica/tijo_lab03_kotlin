package com.company

class MaterialPoint2D(pointX: Double, pointY: Double, mass: Double) : Point2D(pointX, pointY) {

    var mass: Double = 0.toDouble()
        internal set

    init {
        this.mass = mass
    }

    override fun toString(): String {
        return "MaterialPoint2D{" +
                "mass=" + mass +
                ", pointX=" + pointX +
                ", pointY=" + pointY +
                '}'.toString()
    }
}
